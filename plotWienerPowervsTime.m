% =========================================================================
% Author: Daniel Fiallo
% Created: May 24th, 2023
% Last updated: Jun 6th, 2023
% Purpose: The function below takes a file path as an input and plots the
% data in it. The files must be formated as in this folder for the 
% Wiener sheet; https://drive.google.com/drive/folders/17enfPNnkpoCBYytKJlMamlI8hUQhQ1qC?usp=share_link.
% =========================================================================
function plotWienerPowervsTime(wiener_power_file_path)
    % macOS
    % wiener_power_file_path = '/Users/danielfiallo/Documents/MATLAB/ten_min_run (source) - Wiener.xlsx';
    % Windows OS

    % Set options for reading the data
    opts = detectImportOptions(wiener_power_file_path);
    opts.VariableNamingRule = 'preserve';
    
    % Read the data from the Excel file
    data = readtable(wiener_power_file_path, opts);
    
    % Extract the time column
    timeColumn = 'Time';
    timestamp = data.(timeColumn);
    timestamp = datetime(timestamp, 'InputFormat', 'yyyy/MM/dd HH:mm:ss');
    
    % Define the channel names
    channelNames = {'Channel1', 'Channel2', 'Channel3', 'Channel4', 'Channel5', 'Channel6', 'Channel7', 'Channel8', 'Channel9', 'Channel10', ...
                    'Channel11', 'Channel12', 'Channel13', 'Channel14', 'Channel15', 'Channel16', 'Channel17', 'Channel18', 'Channel19', 'Channel20', ...
                    'Channel21', 'Channel22', 'Channel23', 'Channel25', 'Channel26', 'Channel27', 'Channel28', 'Channel29', 'Channel30', ...
                    'Channel31', 'Channel32', 'Channel33'}; % 'Channel24' is not present
    
    % Define a colormap with distinct colors
    numChannels = numel(channelNames);
    colorMap = distinguishable_colors(numChannels);
    
    % Plot the power for each channel
    figure;
    hold on;
    for i = 1:numChannels
        channelVar = [channelNames{i}, ', V [V]'];
        if ismember(channelVar, data.Properties.VariableNames)
            channel_V = extractNumericValue(data.(channelVar));
            channel_I = extractNumericValue(data.([channelNames{i}, ', I [A]']));
            channel_P = channel_V .* channel_I;
            plot(timestamp, channel_P, 'DisplayName', [channelNames{i}, ' Power'], 'Color', colorMap(i, :), 'LineWidth', 2);
        else
            fprintf('Warning: Variable %s not found in the data.\n', channelVar);
        end
    end
    hold off;
    xlabel('Time');
    ylabel('Power [W]');
    title('Wiener Power vs Time');
    legend('Location', 'best');
    grid on;
    
    % Function to extract the numeric value from the string (e.g., '0.016075 V' -> 0.016075)
    function numericValue = extractNumericValue(str)
        str = strrep(str, ' V', '');
        str = strrep(str, ' A', '');
        numericValue = str2double(str);
    end
end