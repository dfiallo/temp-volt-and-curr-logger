function [FWHM, mean] = getFWHMandMean(X, Y)

curve = fit(X', Y', 'Gauss1');
sigma = curve.c1/sqrt(2);
FWHM = 2*sqrt(2*log(2))*sigma;
mean = curve.b1;

end