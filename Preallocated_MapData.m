function [All_CTS, Preallocated_Mapped_Data] = Preallocated_MapData(datafile)
All_CTS=zeros(1,1);
load Array_Maxes_File.mat Array_Maxes;
load Data_Maxes_File.mat Data_Maxes;

Node_Count = Array_Maxes(1);
Board_Count = Array_Maxes(2);
Rena_Count = Array_Maxes(3) ;
Channel_Count = Array_Maxes(4);

%Global_Max_Data = max(Data_Maxes, [], 'all');

Pre_Mapped_Data = cell((Node_Count+1),(Board_Count+1),(Rena_Count+1),(Channel_Count+1));
Pre_Mapped_Data_Count = zeros((Node_Count+1),(Board_Count+1),(Rena_Count+1),(Channel_Count+1));


DataMap = containers.Map();
%{
for Node_Curr = 0:Node_Count
    for Board_Curr = 0:Board_Count
        for Rena_Curr = 0:Rena_Count
            for Channel_Curr = 0:Channel_Count
                
            
                    %{
                    Node_Curr_Char = char(Node_Curr);
                    Board_Curr_Char = char(Board_Curr);
                    Rena_Curr_Char = char(Rena_Curr);
                    Channel_Curr_Char = char(Channel_Curr); 
                    Key_Curr = [Node_Curr_Char, Board_Curr_Char, Rena_Curr_Char, Channel_Curr_Char];
                    %}
                    %Matlab Arrays Start at 1, have to account for
                    Data_Count_Curr = Data_Maxes((Node_Curr+1),(Board_Curr+1),(Rena_Curr+1),(Channel_Curr+1));
                    Value_Curr = zeros(Data_Count_Curr, 5);
                        
                    DataMap(Key_Curr) = Value_Curr;
            end
        end
    end
end
%}
count_init = 0;
disp('Array Init Beginning');
for Node_Curr = 0:Node_Count
    for Board_Curr = 0:Board_Count
        for Rena_Curr = 0:Rena_Count
            for Channel_Curr = 0:Channel_Count
                Data_Count_Curr = Data_Maxes((Node_Curr+1),(Board_Curr+1),(Rena_Curr+1),(Channel_Curr+1));
                Values = zeros(Data_Count_Curr, 5);
                Pre_Mapped_Data{(Node_Curr+1),(Board_Curr+1),(Rena_Curr+1),(Channel_Curr+1)} = Values;
                 count_init = count_init + 1;
                 if (mod(count_init, 1000000) == 0)
                    disp(num2str(count_init));
                    disp((num2str(count_init/111297054)));
                 end
            end
        end
    end
end

rawdata = fopen(datafile);
dataline = fgetl(rawdata);
count = 1;
AllCTS = [];
disp('Data Read Beginning');
while ischar(dataline)
    Data_Array = int64(str2num(dataline));
    
    %was written weird for debugging, fix later
    Node = Data_Array(1);
    Board = Data_Array(2);
    Rena = Data_Array(3);
    Channel = Data_Array(4); 
    %Key = [Node, Board, Rena, Channel];
    
    polarity = Data_Array(5);
    adc_e = Data_Array(6);
    adc_u = Data_Array(7);
    adc_v = Data_Array(8);
    CTS = Data_Array(9);
    All_CTS = cat(1, All_CTS, CTS);
    Value = [polarity,adc_e,adc_u,adc_v,CTS];
    
    Current_Data_Count =  Pre_Mapped_Data_Count((Node+1),(Board+1),(Rena+1),(Channel+1)) +1 ;
    Pre_Mapped_Data_Count((Node+1),(Board+1),(Rena+1),(Channel+1)) =  Current_Data_Count;
    Pre_Mapped_Data{(Node+1),(Board+1),(Rena+1),(Channel+1)}(Current_Data_Count, :) = Value ;

    if (mod(count, 100000) == 0)
        disp(num2str(count));
        disp((num2str(count/111297054)));
    end
    
    count = count + 1;
    dataline = fgetl(rawdata);
end
save('Arrayed_Data_File', 'Pre_Mapped_Data', '-v7.3');
Map_Insertion_Data_Count = zeros((Node_Count+1),(Board_Count+1),(Rena_Count+1),(Channel_Count+1));
disp('Map Insertion Beginning');
count_insert = 0;
for Node_Curr = 0:Node_Count
    for Board_Curr = 0:Board_Count
        for Rena_Curr = 0:Rena_Count
            for Channel_Curr = 0:Channel_Count
                
            
                    
                    Node_Curr_Char = char(Node_Curr);
                    Board_Curr_Char = char(Board_Curr);
                    Rena_Curr_Char = char(Rena_Curr);
                    Channel_Curr_Char = char(Channel_Curr); 
                    Key_Curr = [Node_Curr_Char, Board_Curr_Char, Rena_Curr_Char, Channel_Curr_Char];
                    
                    Current_Count = Map_Insertion_Data_Count((Node_Curr+1),(Board_Curr+1),(Rena_Curr+1),(Channel_Curr+1)) + 1;
                    Map_Insertion_Data_Count((Node_Curr+1),(Board_Curr+1),(Rena_Curr+1),(Channel_Curr+1)) = Current_Count;
                    %Matlab Arrays Start at 1, have to account for
                    Data_Count_Curr = Data_Maxes((Node_Curr+1),(Board_Curr+1),(Rena_Curr+1),(Channel_Curr+1));      
                    DataMap(Key_Curr) = Pre_Mapped_Data{(Node_Curr+1),(Board_Curr+1),(Rena_Curr+1),(Channel_Curr+1)}(:,:);
                    count_insert = count_insert + 1;
                    if (mod(count_insert, 1000000) == 0)
                        disp(num2str(count_insert));
                        disp((num2str(count_insert/111297054)));
                    end
            end
        end
    end
end

save('Mapped_Data_File', 'DataMap', '-v7.3');

Preallocated_Mapped_Data = DataMap;
fclose(rawdata);
end