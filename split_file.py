import os

def split_file(input_file, output_directory, chunk_size):
    file_name = os.path.basename(input_file)
    file_size = os.path.getsize(input_file)
    chunk_bytes = chunk_size * 1024 * 1024

    with open(input_file, 'rb') as f_in:
        index = 0
        while True:
            data = f_in.read(chunk_bytes)
            if not data:
                break

            output_file = os.path.join(output_directory, f'part{index + 1}_{file_name}')
            with open(output_file, 'wb') as f_out:
                f_out.write(data)

            index += 1

            # Optional: Print progress
            print(f'Splitting file: {index} / {file_size // chunk_bytes + 1}')

    print('File splitting complete!')

# Usage example
input_file_path = 'E:/20230512_mouse_phantom/test_5_30min/data_20230512_161055.dat'
output_directory_path = 'E:/20230512_mouse_phantom/test_5_30min/SmallerFiles'
chunk_size_mb = 500

split_file(input_file_path, output_directory_path, chunk_size_mb)