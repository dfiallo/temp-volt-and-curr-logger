function [Count, All_CTS, CompleteMap] = NewMap(datafile)


disp('Finding Data Ranges');
[~, Count]=Max_Array_Size(datafile)
disp('Input Ranges Complete');

disp('Finding Input Ranges');
Max_Data_Size(datafile)
disp('Input Ranges Complete');

disp('Beginning Mapping Process');
[All_CTS, CompleteMap] = Preallocated_MapData(datafile);
disp('Mapping Complete');
end