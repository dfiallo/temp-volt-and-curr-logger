function [outputArg1] = plotdata(nodeNumber, boardNumber, map)



for a=0:nodeNumber
    for b=30:-1:(30+1-boardNumber)
        i=0;
        j=0;

        name = ['node', num2str(a),'board', num2str(b)];

        for rena=0:1
            figure('Name',name);
            if rena ==0
                for channel=4:1:28
                    [Accessed_Data] = AccessMap(a,b,rena,channel,map);
                    j=j+1;
                    label = ['R', num2str(rena), 'C', num2str(channel)]; 
                    
                    Accessed_Data(:,2);
                              
                    [BinCounts, BinEdges] = histcounts(Accessed_Data(:,2), [0:10:4000]);
                
                    BinCenters = conv(BinEdges, [0.5 0.5], 'valid');
                
                    %curve = fit(BinCenters', BinCounts', 'Gauss1');
                
                    c = subplot(5,5,j);
                    histogram(Accessed_Data(:,2),[0:10:4000]);
                    hold on
                    %plot(curve);
                    legend('off');
                    title(label);
                    hold off
                
                end
            elseif rena ==1
                for channel=7:1:28
                    [Accessed_Data] = AccessMap(a,b,rena,channel,map);
                    i=i+1;
                    label = ['R', num2str(rena), 'C', num2str(channel)]; 
                    
                    Accessed_Data(:,2);
                              
                    [BinCounts, BinEdges] = histcounts(Accessed_Data(:,2), [0:10:4000]);
                
                    BinCenters = conv(BinEdges, [0.5 0.5], 'valid');
                
                    %curve = fit(BinCenters', BinCounts', 'Gauss1');
                
                    c = subplot(5,5,i);
                    histogram(Accessed_Data(:,2),[0:10:4000]);
                    hold on
                    %plot(curve);
                    legend('off');
                    title(label);
                    hold off
                
                end
                
            end
            filename = ['D:\PostDocWork\ScaleUpTesting\Panel2\RENAtesting4\',name, label];
            savefig(filename);
            saveas(gcf,filename, 'jpg');
            close;
        end
    end
end

outputArg1 = 'finished';
end

