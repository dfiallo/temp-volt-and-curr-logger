#######################################################################
# Zachary Bethel
# Keysight Technologies N5743A Voltage and Current DAQ
# CAPSTONE PROJECT
# Prof. Shiva Abbaszadeh
#
# Requirements:
# WINDOWS 10 -- DOES NOT OPERATE ON WINDOWS 11
# Requires the Keysight IO Library Suite ( This is running on Ver: Update 2, 2022)
# Requires Internet connection
#######################################################################



from datetime import datetime
import time

import paramiko

import gspread

import pyvisa
import subprocess

KEYSIGHTheaders = ["Time",
                   "Voltage1", "Current1",
                   "Voltage2", "Current2",
                   "Voltage3", "Current3",
                   "Voltage4", "Current4",
                   "Voltage5", "Current5"]

WIENERheaders = ["Time",
                 "Channel1, V [V]", "Channel1, I [A]",
                 "Channel2, V [V]", "Channel2, I [A]",
                 "Channel3, V [V]", "Channel3, I [A]",
                 "Channel4, V [V]", "Channel4, I [A]",
                 "Channel5, V [V]", "Channel5, I [A]",
                 "Channel6, V [V]", "Channel6, I [A]",
                 "Channel7, V [V]", "Channel7, I [A]",
                 "Channel8, V [V]", "Channel8, I [A]",
                 "Channel9, V [V]", "Channel9, I [A]",
                 "Channel10, V [V]", "Channel10, I [A]",
                 "Channel11, V [V]", "Channel11, I [A]",
                 "Channel12, V [V]", "Channel12, I [A]",
                 "Channel13, V [V]", "Channel13, I [A]",
                 "Channel14, V [V]", "Channel14, I [A]",
                 "Channel15, V [V]", "Channel15, I [A]",
                 "Channel16, V [V]", "Channel16, I [A]",
                 "Channel17, V [V]", "Channel17, I [A]",
                 "Channel18, V [V]", "Channel18, I [A]",
                 "Channel19, V [V]", "Channel19, I [A]",
                 "Channel20, V [V]", "Channel20, I [A]",
                 "Channel21, V [V]", "Channel21, I [A]",
                 "Channel22, V [V]", "Channel22, I [A]",
                 "Channel23, V [V]", "Channel23, I [A]",
                 "Channel25, V [V]", "Channel25, I [A]",
                 "Channel26, V [V]", "Channel26, I [A]",
                 "Channel27, V [V]", "Channel27, I [A]",
                 "Channel28, V [V]", "Channel28, I [A]",
                 "Channel29, V [V]", "Channel29, I [A]",
                 "Channel30, V [V]", "Channel30, I [A]",
                 "Channel31, V [V]", "Channel31, I [A]",
                 "Channel32, V [V]", "Channel32, I [A]",
                 "Channel33, V [V]", "Channel33, I [A]"]

##teamDrive_ID = '0AIKKEuf6vKbGUk9PVA'
teamFolder_ID = '15JMv6LD5YmShitQb02xhAhryQ27Fvvy0'

#Function getTime() to output formated RT string 
def getTime():
    
    #Capture the instatnenous time from time modual which captures the 
    # SYSTEM_CLOCK from time.pyi with relative timezone options
    now=datetime.now()
    
    # use strftime("") to create a string that represents desired date 
    # and time information
    # %Y-> year
    # %m-> month
    # %d-> day
    # %H-> hour
    # %M-> minute
    # %S-> second
    dt_string = now.strftime("%Y/%m/%d %H:%M:%S")
    
    #output formatted string
    return dt_string

def getLocation(ID_holder):
    if ID_holder == '28-00000f1f4c2f':
        return 'Panel2Node7RENA1ASIC0'#c2f
    if ID_holder == '28-00000f1f5e66':
        return 'Panel2CZT0'#e66
    elif ID_holder == '28-00000f1f40af':
        return 'Panel1Node2ZED2'#0af
    elif ID_holder == '28-00000f1f65f0':
        return 'Panel1Node0RENA1ASIC0'#5f0
    elif ID_holder == '28-00000f1f528b':
        return 'Panel1Node1RENA1ASIC0'#28b
    elif ID_holder == '28-00000f1f6436':
        return 'Panel1Node2RENA1ASIC0'#436
    elif ID_holder == '28-00000f1f4429':
        return 'Panel2Node7ZED7'#429maybewrong
    elif ID_holder == '28-00000f1f5f7a':
        return 'Panel1CZT0'#f7a
    elif ID_holder == '28-00000f1f4eb8':
        return 'Panel1Node4ZED4'#eb8
    elif ID_holder == '28-00000f1f56ee':
        return 'Panel2Node9RENA1ASIC0'#6ee
    elif ID_holder == '28-00000f1f63f6':
        return 'Panel1Node3RENA1ASIC0'#3f6
    elif ID_holder == '28-00000f1f40c9':
        return 'Panel1Node4RENA1ASIC0'#0c9
    elif ID_holder == '28-00000f1f5116':
        return 'Panel2Node5RENA1ASIC0'#116
    elif ID_holder == '28-00000f1f4a0f':
        return 'Panel2Node5ZED5'#a0fmaybewrong
    elif ID_holder == '28-00000f1f4564':
        return 'Panel1Node0ZED0'#564
    elif ID_holder == '28-00000f1f4403':
        return 'Panel2Node6RENA1ASIC0'#403
    elif ID_holder == '28-00000f1f41d8':
        return 'Panel1CZT1'#1d8
    elif ID_holder == '28-00000f1f4e8e':
        return 'Panel2Node8RENA1ASIC0'#e8e
    elif ID_holder == '28-00000f1f5827':
        return 'Panel2CZT1' #827
    elif ID_holder == '28-00000f1f5571':
        return 'Panel2Node9ZED9' #571
    else:
        return 'ID_LOCATION_ERROR'

def temperatureHEADERS(ID_list, worksheetHolder):
    row_position=1
    n= len(ID_list)+1

    #INSERT HEADERS
    for i in range(n):
            if i == 0:
                worksheetHolder.update_cell(row_position,1,'Locations')
            else:
                worksheetHolder.update_cell(row_position,(i+1), getLocation(ID_list[i-1]))
    row_position= row_position+1
    
    
    #INSERT HEADERSID_list
    for i in range(n):
            if i == 0:
                 worksheetHolder.update_cell(row_position,1,'IDs')
            else:
                worksheetHolder.update_cell(row_position,(i+1), ID_list[i-1])
    row_position= row_position+1
    
    #INSERT HEADERS
    for i in range(n):
            if i == 0:
                worksheetHolder.update_cell(row_position,1,'Time')
            else:
                worksheetHolder.update_cell(row_position,(i+1), 'Temperature [C]')
    row_position= row_position+1

    return

def SNMPtoWiener():
        # SNMP command to execute
    snmp_command = ['snmpwalk','-Cp','-Oqv', '-v', '2c', '-m', '+WIENER-CRATE-MIB','-c', 'public', '169.254.222.24','outputMeasurementSenseVoltage']

    # Execute the SNMP command and capture the output
    output = subprocess.check_output(snmp_command)


    # Decode the output as a string
    output_str = output.decode('utf-8')

    # Split the string by the delimiter '\r\n'
    values = output_str.split('\r\n')

    # Remove the last empty element
    voltage = values[:-2]


    # SNMP command to execute
    snmp_command = ['snmpwalk','-Cp','-Oqv', '-v', '2c', '-m', '+WIENER-CRATE-MIB','-c', 'public', '169.254.222.24','outputMeasurementCurrent']

    # Execute the SNMP command and capture the output
    output = subprocess.check_output(snmp_command)


    # Decode the output as a string
    output_str = output.decode('utf-8')

    # Split the string by the delimiter '\r\n'
    values = output_str.split('\r\n')

    # Remove the last empty element
    current = values[:-2]

    # Merge arrays
    merged_array = []
    for a, b in zip(voltage, current):
        merged_array.append(a)
        merged_array.append(b)
##
##    # Convert merged array to string
##    merged_string = ','.join(merged_array)
    return merged_array

def OnwWireTempSensorsReadTemp(client, tempIDs):
    tempTemps=[] 
    # Read data from each device
    for tempID in tempIDs:
        device_path = '/sys/bus/w1/devices/' + tempID + '/w1_slave'
        command = 'cat ' + device_path
        stdin, stdout, stderr = client.exec_command(command)
        device_output = stdout.read().decode().strip()

        # Perform CRC check
        lines = device_output.split('\n')
        if len(lines) == 2 and lines[0].strip()[-3:] == 'YES':
            
            temperature_line = lines[1].strip()
            temperature_data = temperature_line.split('=')[1]
            tempTemp = float(temperature_data) / 1000.0
            # Process the device output as per your requirement

            if int(tempTemp) == 85:
                tempTemps.append("SENSOR ERROR!")
            else:
                tempTemps.append(tempTemp)
        else:
            print()
            tempTemps.append("CRC ERROR!")
    return tempTemps

def GetTimePerDataCall():
    while True:
        dataInterval = input("Enter the amount of time [sec, integer] to take data from PET system [>=60sec]: ")
        try:
            dataInterval = int(dataInterval)
            if dataInterval >= 60:
                break  # Exit the loop if the input is an integer
            else:
                print("Input should be greater than 60 seconds to reset API calls.")
        except ValueError:
            print("Invalid input. Please enter an integer.")
    return dataInterval

def OnwWiretempSensorsReadID(client):
    # Read 1-wire devices
    command = 'ls /sys/bus/w1/devices/'
    stdin, stdout, stderr = client.exec_command(command)

    # Process the output
    output = stdout.readlines()

    device_dirs = []
    # Extract the device directories
    device_dirs = [line.strip() for line in output if line.startswith('28-')]

    if len(device_dirs) == 1:
        device_ids.append("The array is empty.")
    else:
        # Array to store device IDs
        device_ids = []

        # Read ID from each device
        for device_dir in device_dirs:
            device_ids.append(device_dir)
    return device_ids

gc = gspread.oauth()
# SSH connection details
hostname = '128.114.51.229'  # Replace with the actual hostname or IP address
username = 'logger'  # Replace with your SSH username
password = 'shiva'  # Replace with your SSH password

# SSH connection setup
client = paramiko.SSHClient()
client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
client.connect(hostname, username=username, password=password)

#create spreadsheet
Spreadsheet_name = input("Enter Spreadsheet name for Keysight power supply data: ")

#Create Keysight spreadsheet on first sheet
spreadsheet=gc.create(Spreadsheet_name, folder_id = teamFolder_ID)
keysightWRKST = spreadsheet.sheet1
#rename sheet 1 to identify Keysight
keysightWRKST.update_title('Keysight')
keysightWRKST.format('B2:K',{'numberFormat':{'type':'NUMBER','pattern':"#0.000#"}})

#Create wiener worksheet
wienerWRKST = spreadsheet.add_worksheet(title="Wiener", rows=1000, cols=100)
wienerWRKST.format('B2:K',{'numberFormat':{'type':'NUMBER','pattern':"#0.000000"}})

#create Keysight worksheet
tempWRKST = spreadsheet.add_worksheet(title="Temp", rows=1000, cols=35)
tempWRKST.format('B4:U',{'numberFormat':{'type':'NUMBER','pattern':"#00.0#"}})

#create keysight and wiener headers
row_index = 1
for i in range(len(KEYSIGHTheaders)):
    keysightWRKST.update_cell(row_index, (i+1), KEYSIGHTheaders[i])
for i in range(len(WIENERheaders)):
    wienerWRKST.update_cell(row_index, (i+1), WIENERheaders[i])

#create array of one wire IDs
DS18B20PAR_IDs = []
DS18B20PAR_IDs = OnwWiretempSensorsReadID(client)

##Creating headers and creating first api call will max out API call qouta of 60 calls per 60 seconds. Implement this here to 
time.sleep(30)

#create temperature headers
temperatureHEADERS(DS18B20PAR_IDs,tempWRKST)

row_index= row_index + 1

##Creating headers and creating first api call will max out API call qouta of 60 calls per 60 seconds. Implement this here to 
time.sleep(60)

#Search device for VISA enabled instruments
rm = pyvisa.ResourceManager()
##print(rm.list_resources())

#The string assigns a variable class (class could be wrong terminology here) to the exact Keysight N5743A documents
Keysight1 = rm.open_resource('USB0::0x0957::0x9407::US22D3620R::0::INSTR')
Keysight2 = rm.open_resource('USB0::0x0957::0x9407::US21J0118R::0::INSTR')
Keysight3 = rm.open_resource('USB0::0x0957::0x9407::US22D3619R::0::INSTR')
Keysight4 = rm.open_resource('USB0::0x0957::0x9407::US15J9517P::0::INSTR')
Keysight5 = rm.open_resource('USB0::0x0957::0x9407::US20A1568P::0::INSTR')
##print(Keysight1)

dataInterval = GetTimePerDataCall()

while True:
    ##print(Keysight1.query("*IDN?"))

    #call the Keysight power supplies
    keysightWRKST.update_cell(row_index, 1, getTime())
    keysightWRKST.update_cell(row_index, 2, Keysight1.query("MEASure:VOLTage?"))
    keysightWRKST.update_cell(row_index, 3, Keysight1.query("MEASure:CURRent?"))
    keysightWRKST.update_cell(row_index, 4, Keysight2.query("MEASure:VOLTage?"))
    keysightWRKST.update_cell(row_index, 5, Keysight2.query("MEASure:CURRent?"))
    keysightWRKST.update_cell(row_index, 6, Keysight3.query("MEASure:VOLTage?"))
    keysightWRKST.update_cell(row_index, 7, Keysight3.query("MEASure:CURRent?"))
    keysightWRKST.update_cell(row_index, 8, Keysight4.query("MEASure:VOLTage?"))
    keysightWRKST.update_cell(row_index, 9, Keysight4.query("MEASure:CURRent?"))
    keysightWRKST.update_cell(row_index, 10, Keysight5.query("MEASure:VOLTage?"))
    keysightWRKST.update_cell(row_index, 11, Keysight5.query("MEASure:CURRent?"))

    #call for the Wiener voltage and power array
    WienerArray = ""
    WienerArray = SNMPtoWiener()

    # Print the time
    wienerWRKST.update_cell(row_index, 1, getTime())
    # Print the extracted Wiener power values
    for i in range(len(WienerArray)):
        wienerWRKST.update_cell(row_index, i+2, WienerArray[i])

    #call temperature sensors for temeprature values
    DS18B20PAR_temps = OnwWireTempSensorsReadTemp(client, DS18B20PAR_IDs)
    
    tempWRKST.update_cell(row_index+2, 1, getTime())
    # Print the extracted values
    for i in range(len(DS18B20PAR_temps)):
        tempWRKST.update_cell(row_index+2, i+2, DS18B20PAR_temps[i])
    
    row_index= row_index + 1

    time.sleep(int(dataInterval))

# Close the SSH connection
client.close()
