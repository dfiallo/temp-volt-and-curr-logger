function [Data_Maxes] = Max_Data_Size(datafile)

load Array_Maxes_File.mat Array_Maxes;

Node_Count = Array_Maxes(1) + 1;
Board_Count = Array_Maxes(2) + 1;
Rena_Count = Array_Maxes(3) + 1;
Channel_Count = Array_Maxes(4) + 1;

Full_Data_Array = zeros(Node_Count,Board_Count,Rena_Count, Channel_Count);

rawdata = fopen(datafile);
dataline = fgetl(rawdata);
count = 1;
%111297054
while ischar(dataline)
    
    Data_Array = int64(str2num(dataline));
    
    %was written weird for debugging, fix later
    Node = Data_Array(1) + 1;
    Board = Data_Array(2) + 1;
    Rena = Data_Array(3) + 1; 
    Channel = Data_Array(4) + 1;
    Full_Data_Array(Node,Board,Rena,Channel) = Full_Data_Array(Node,Board,Rena,Channel) + 1;

    if (mod(count, 100000) == 0)
        disp(num2str(count));
    end
    dataline = fgetl(rawdata);
    count = count + 1;
    
 end

Data_Maxes = Full_Data_Array;
save('Data_Maxes_File', 'Data_Maxes');
fclose(rawdata);
end
       
