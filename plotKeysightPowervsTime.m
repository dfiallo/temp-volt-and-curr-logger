% =========================================================================
% Author: Daniel Fiallo
% Created: May 24th, 2023
% Last updated: Jun 6th, 2023
% Purpose: The function below takes a file path as an input and plots the
% data in it. The files must be formated as in this folder for the 
% Keysight sheet; https://drive.google.com/drive/folders/17enfPNnkpoCBYytKJlMamlI8hUQhQ1qC?usp=share_link.
% =========================================================================
function plotKeysightPowervsTime(keysight_power_file_path)
    % macOS
    % keysight_power_file_path = '/Users/danielfiallo/Documents/MATLAB/ten_min_run (source) - Keysight.xlsx';

    % Read the data from the Excel file
    data = readtable(keysight_power_file_path);

    % Extract the timestamp and power columns
    timeColumn = 'Time';
    powerColumns = {'Voltage1', 'Current1', 'Voltage2', 'Current2', 'Voltage3', 'Current3', 'Voltage4', 'Current4', 'Voltage5', 'Current5'};

    timestamp = data.(timeColumn);
    power = data{:, powerColumns};

    % Convert timestamp to MATLAB datetime format with specified format
    timestamp = datetime(timestamp, 'InputFormat', 'yyyy/MM/dd HH:mm:ss');

    % Calculate power as the product of voltage and current
    numLocations = numel(powerColumns) / 2; % Assuming there are pairs of voltage and current columns for each location
    power = zeros(size(data, 1), numLocations);

    for i = 1:numLocations
        voltageColumn = powerColumns{(i - 1) * 2 + 1};
        currentColumn = powerColumns{i * 2};
        power(:, i) = data.(voltageColumn) .* data.(currentColumn);
    end

    % Create a table to store the power data
    powerTable = table(timestamp);
    for i = 1:numLocations
        powerColumnName = sprintf('Power%d', i);
        powerTable.(powerColumnName) = power(:, i);
    end

    % Plot the power versus time for each location
    figure;
    hold on;

    % Define the node information for each Keysight device
    nodeInfo = {'Node 9', 'Nodes 5,7','Nodes 6,8', 'Nodes 0,1', 'Nodes 2,3,4'};

    % Iterate over each location
    for i = 1:numLocations
        plot(timestamp, power(:, i), 'LineWidth', 2, 'DisplayName', sprintf('Keysight %d - %s', i, nodeInfo{i}));
    end

    hold off;

    % Set the plot title and axis labels
    % Radiation
    % titleText = 'Keysight Power Supplies Power Rating Over a Period of 60 Minutes';
    % subtitleText = 'Experiment using Germanium (Ge-68) as a Radiation Source';
    % Pulse
    titleText = 'Keysight Power Supplies for Panel 1 Over a Period of 60 Minutes';
    subtitleText = 'Experiment using 1kHz 250mVpp 50% Duty Cycle Sqaure Wave';
    combinedTitle = sprintf('%s\n%s', titleText, subtitleText);
    title(combinedTitle, 'FontSize', 18)
    xlabel('Time [HH:mm]', 'FontSize', 18);
    ylabel('Power [W]', 'FontSize', 18);
    grid on;
    
    % Increase the font size of tick labels on x-axis
    set(gca, 'FontSize', 18) % 'gca' returns the current axes handle
    
    % Display the legend with distinct colors
    lgd = legend('Location', 'northwest', 'FontSize', 16);
    legend('boxoff');
    lgd.Title.String = 'Locations';
    lgd.Title.FontSize = 16;

    % Save the power data to a .csv file
    output_file_path = 'power_table.csv';
    writetable(powerTable, output_file_path);
end