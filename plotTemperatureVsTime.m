% =========================================================================
% Author: Daniel Fiallo
% Created: May 24th, 2023
% Last updated: May 29th, 2023
% Purpose: The function below takes a file path as an input and plots the
% data in it. The files must be formated as in this folder for the 
% temperature sheet; https://drive.google.com/drive/folders/17enfPNnkpoCBYytKJlMamlI8hUQhQ1qC?usp=share_link.
% =========================================================================
function plotTemperatureVsTime(temperature_file_path)
    % macOS file path format
    % temperature_file_path = '/Users/danielfiallo/Documents/MATLAB/hour_run (source) - Temp.xlsx';
    % WindowsOS file path format

    
    % Read the data from the Excel file
    data = readtable(temperature_file_path);

    % Extract the timestamp and temperature columns
    timeColumn = data.Properties.VariableNames{1};
    temperatureColumns = data.Properties.VariableNames(2:end);
    timestamp = data.(timeColumn);
    temperature = data{:, temperatureColumns};

    % Convert timestamp to MATLAB datetime format with specified format
    timestamp = datetime(timestamp, 'InputFormat', 'yyyy/MM/dd HH:mm:ss');

    % Save the temperature data to a .dat file
    fileID = fopen('temperature_file.dat', 'w');
    fprintf(fileID, 'Time\t%s\n', strjoin(temperatureColumns, '\t'));

    for i = 1:numel(timestamp)
        fprintf(fileID, '%s', datestr(timestamp(i), 'yyyy/mm/dd HH:MM:SS'));
        fprintf(fileID, '\t%.3f', temperature(i, :));
        fprintf(fileID, '\n');
    end

    fclose(fileID);

    % Plot the temperature versus time for each location
    figure;
    hold on;
    
    % Define the legend entries and desired colors
    legendEntries = temperatureColumns;
    colors = [
        1 0 0;      % Panel1Node2ZED2       - Red
        1 0.5 0;    % Panel1Node4RENA1ASIC0 - Orange
        1 1 0;      % Panel1CZT1            - Yellow
        0 1 0;      % Panel1Node0ZED0       - Green
        0 0 1;      % Panel1Node4ZED4       - Blue
        0.5 0 1;    % Panel1Node1RENA1ASIC0 - Purple
        0.5 0.5 0;  % Panel1CZT0            - Olive
        1 0.8 0.6;  % Panel1Node3RENA1ASIC0 - Peach
        1 0 1;      % Panel1Node2RENA1ASIC0 - Magenta
        0 1 1;      % Panel1Node0RENA1ASIC0 - Cyan
    ];
    
    % Iterate over each location
    for i = 1:size(temperature, 2)
        plot(timestamp, temperature(:, i), 'LineWidth', 2, 'DisplayName', ...
            sprintf('%s', temperatureColumns{i}), 'Color', colors(i, :));
    end
    
    hold off;
    
    % Plot title and axis labels
    % Radiation
    % titleText = 'Temperature at Specified Locations Within the PET System Over a Period of 60 Minutes';
    % subtitleText = 'Experiment using Germanium (Ge-68) as a Radiation Source';
    % Pulse
    titleText = 'Temperature at Specified Locations Within the PET System Over a Period of 60 Minutes';
    subtitleText = 'Experiment using 1kHz 250mVpp 50% Duty Cycle Sqaure Wave';
    combinedTitle = sprintf('%s\n%s', titleText, subtitleText);
    title(combinedTitle, 'FontSize', 18)
    xlabel('Time [HH:mm]', 'FontSize', 18);
    ylabel('Temperature [˚C]', 'FontSize', 18);
    grid on;

    % Increase the font size of tick labels on x-axis
    set(gca, 'FontSize', 18) % 'gca' returns the current axes handle
    
    % Display the legend with distinct colors
    lgd = legend(legendEntries, 'Location', 'northwest', 'FontSize', 16);
    legend('boxoff');
    lgd.Title.String = 'Locations';
    lgd.Title.FontSize = 16;
end