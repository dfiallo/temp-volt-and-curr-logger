function [outputArray, maxminFWHM] = channelSort(nodeNumber, boardNumber, map)
%UNTITLED7 Summary of this function goes here
%   Detailed explanation goes here

FWHM = [];

board = [];
meanC=[];
names=[];
maxFWHM = [];
minFWHM=[];
avgFWHM=[];

a=nodeNumber;

    name1=['n', num2str(a)];
    
    for b=30:-1:(30-boardNumber+1)
        FWHMcheck =[];
        
        if b < 10
            name2=[name1, 'b' , '0', num2str(b)];
        else
            name2=[name1, 'b' , num2str(b)];
        end
        for rena=0:1
            
            if rena ==0
                
                for channel=4:1:28
                    if channel < 10
                        name3 = [name2,'r', num2str(rena), 'c0', num2str(channel)];
                        names=[names;name3];
                        
                    else
                        name3 = [name2,'r', num2str(rena), 'c', num2str(channel)];
                        names=[names;name3];
                        
                    end
                    [Accessed_Data] = AccessMap(a,b,rena,channel,map);
                    
                    [BinCounts, BinEdges] = histcounts(Accessed_Data(:,2), [0:10:4000]);
                    BinCenters = conv(BinEdges, [0.5 0.5], 'valid');
                    [FWHMholder, meanholder] = getFWHMandMean(BinCenters, BinCounts);
                    FWHMcheck=[FWHMcheck;FWHMholder];
                    FWHM = [FWHM; FWHMholder];
                    meanC = [meanC; meanholder];
                end
            elseif rena ==1
                for channel=7:1:28
                    if channel < 10
                        name3 = [name2,'r', num2str(rena), 'c0', num2str(channel)];
                        names=[names;name3];
                    else
                        name3 = [name2,'r', num2str(rena), 'c', num2str(channel)];
                        names=[names;name3];
                        
                    end
                    [Accessed_Data] = AccessMap(a,b,rena,channel,map);
                    
                    [BinCounts, BinEdges] = histcounts(Accessed_Data(:,2), [0:10:4000]);
                    BinCenters = conv(BinEdges, [0.5 0.5], 'valid');
                    [FWHMholder, meanholder] = getFWHMandMean(BinCenters, BinCounts);
                    FWHMcheck=[FWHMcheck;FWHMholder];
                    FWHM = [FWHM; FWHMholder];
                    meanC = [meanC; meanholder];
                    
                end
            end
        end
        board = [board; b];
        avg=mean(FWHMcheck, 'all');
        avgFWHM=[avgFWHM; avg];
        maxFWHM=[maxFWHM; max(FWHMcheck)];
        minFWHM=[minFWHM; min(FWHMcheck)];
        
        
    end


maxminFWHM = cat(2, board, maxFWHM, minFWHM, avgFWHM);    
outputArray = {names, FWHM, meanC};
end

