function [Array_Maxes, Line_Count] = Max_Array_Size(datafile)


rawdata = fopen(datafile);
Line_Count = 0;
Node_Count = 0;
Board_Count = 0;
Rena_Count = 0;
Channel_Count = 0;
dataline = fgetl(rawdata);

while ischar(dataline)
    Data_Array = int64(str2num(dataline));
     Curr_Node = Data_Array(1);
     Curr_Board = Data_Array(2);
     Curr_Rena = Data_Array(3);
     Curr_Channel = Data_Array(4);

    Node_Count = max(Node_Count, Curr_Node);
    Board_Count = max(Board_Count, Curr_Board);
    Rena_Count = max(Rena_Count, Curr_Rena);
    Channel_Count = max(Channel_Count, Curr_Channel);
    Line_Count = Line_Count + 1;
    dataline = fgetl(rawdata);
     if (mod(Line_Count, 100000) == 0)
        disp(num2str(Line_Count));
    end
 end
Array_Maxes = [Node_Count, Board_Count, Rena_Count, Channel_Count];
save('Array_Maxes_File', 'Array_Maxes');
fclose(rawdata);
end